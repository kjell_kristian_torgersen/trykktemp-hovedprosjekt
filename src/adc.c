#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "main.h"

#include "useful.h"
#include "api/timer.h"

#define ADC_CHANNELS 4
#define ADC_AVERAGES 4096
#define ADC_SAMPLES 128

extern ADC_HandleTypeDef hadc1;

static uint32_t adcSums[ADC_CHANNELS];
static uint16_t adcSamples[ADC_SAMPLES*ADC_CHANNELS];
static bool newSamples = false;
static uint32_t adccount = 0;
static float voltages[ADC_CHANNELS];
static volatile uint32_t timertest=0;

void adc_init(void)
{
	//HAL_ADC_Start_DMA(&hadc1, (void*)adcSamples, 4);
	//HAL_ADC_Start_DMA(&hadc1, (void*)adcSamples, ADC_SAMPLES*ADC_CHANNELS);
}

void adc_start(void)
{
	HAL_ADC_Start_DMA(&hadc1, (void*)adcSamples, ADC_SAMPLES*ADC_CHANNELS);
	//HAL_ADC_Start_IT(&hadc1);
}

void adc_process(uint16_t * samples, int count)
{
	for(int j = 0; j < count; j++) {
		for(int i = 0; i < ADC_CHANNELS; i++) {
			uint32_t tmp = (uint32_t)samples[ADC_CHANNELS * j + i];
			adcSums[i] += tmp;
		}
		adccount++;
		if(adccount >= ADC_AVERAGES) {
			HAL_ADC_Stop_DMA(&hadc1);
			//HAL_ADC_Stop_IT(&hadc1);
			timertest = timer_ms();
			adccount = 0;
			for(int i = 0; i < ADC_CHANNELS; i++) {
				voltages[i] = 3.3f * adcSums[i] / ((float)ADC_AVERAGES * 4096.0f);
				adcSums[i] = 0;
			}
			newSamples = true;
		}
	}
}

/** \brief Process first half of buffer */
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc)
{
	adc_process(&adcSamples[0], ADC_SAMPLES/2);
}

/** \brief Process last half of buffer */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	adc_process(&adcSamples[ADC_SAMPLES*ADC_CHANNELS/2], ADC_SAMPLES/2);
}


float * adc_newSamples(void)
{
	if(newSamples) {
		newSamples = false;
		return voltages;
	}
	return NULL;
}
