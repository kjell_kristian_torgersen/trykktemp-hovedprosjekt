#include <math.h>

#include "api/timer.h"
#include "api/gpio.h"
#include "api/delay.h"

#include "useful.h"

#include "main.h"
#include "adc.h"
#include "drivers/onewire/onewire.h"
#include "crc8.h"

static uint32_t timer = 0;
extern uint32_t tim2_ticks;

static onewire_t ow;

#define DS18X20_CONVERT_T         0x44
#define DS18X20_READ              0xBE
#define DS18X20_SP_SIZE           9

void app_main_setup(void)
{
	useful_init();
	delay_init();
	adc_init();
	uart_printf("Program started...\r\n");
	onewire_init(&ow, 1, 0);
}

static int state = 0;

#define CRC8INIT    0x00
#define CRC8POLY    0x18              //0X18 = X^8+X^5+X^4+X^0

uint16_t gettemp(uint8_t id[])
{
	uint8_t sp[9];
	uint8_t i;
	crc_t crc = crc_init();

	onewire_command(&ow, DS18X20_READ, id);
	for (i = 0; i < sizeof(sp); i++) {
		sp[i] = onewire_readByte(&ow);
	}
	crc = crc_update(crc, sp, 8);
	crc = crc_finalize(crc);

	return sp[0] + (sp[1]<<8);
}

void app_main_loop(void)
{
	uint8_t sensors[5][8];
	switch (state) {
	case 0:
		if (timer_ms() - timer >= 1000) {
			timer = timer_ms();
			gpio_togglePin(2, 13);
			adc_start();
			state = 1;
		}
		break;
	case 1: {
		float *samples = adc_newSamples();
		if (samples) {
			// perform temperature measurement read out
			float adctemp = (samples[3] - 1.319674f) / 0.0043f + 25.0f; //
			float temps[3] = {0};
			// collect temperature measurements
			int nSensors = onewire_searchSensors(&ow, sensors, 5);
			//uart_printf("nSensors = %i\r\n", nSensors);
			for (int i = 0; i < nSensors; i++) {
			/*	uart_printf("%i: ", i);
				for (int j = 0; j < 8; j++) {
					uart_printf("%02X ", sensors[i][j]);
				}
				uart_printf(": ");*/
				//onewire_command(&ow, 0xBE, sensors[i]);
				/*read_scratchpad(sensors[i], sp, sizeof(sp));
				for (int j = 0; j < sizeof(sp); j++) {
					uart_printf("%02X ", sp[j]);
				}*/
				temps[i] = gettemp(sensors[i]) / 16.0f;

				//uart_printf("%0.1f\r\n", temp / 16.0f);
			}
			onewire_command(&ow, 0x44, NULL);
			uart_printf("%0.6f %0.6f %0.6f %0.6f %u %0.1f %0.1f %0.1f %0.1f\r\n", 25.5f*samples[0]/15.5f, 25.5f*samples[1]/15.5f, 25.5f*samples[2]/15.5f, samples[3], tim2_ticks, adctemp, temps[0], temps[1], temps[2]);
			state = 0;
		}
	}
		break;
	default:
		state = 0;
		break;
	}
}
