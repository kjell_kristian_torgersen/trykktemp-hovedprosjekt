/*
 * adc.h
 *
 *  Created on: Oct 25, 2020
 *      Author: kjell
 */

#ifndef ADC_H_
#define ADC_H_

void adc_init(void);
void adc_start(void);
float * adc_newSamples(void);

#endif /* ADC_H_ */
