#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

#include "api/uart.h"


static uart_t * uart;

static uint8_t rxbuf[64];
static uint8_t txbuf[256];

extern UART_HandleTypeDef huart1;

extern uart_t* uart_init(UART_HandleTypeDef *huart, uint32_t baudRate, void *rxbuf, sz_t rxbufsize, void *txbuf, sz_t txbufsize);

void useful_init(void)
{
	uart = uart_init(&huart1, 115200, rxbuf, sizeof(rxbuf), txbuf, sizeof(txbuf));
}

int uart_printf(const char *__restrict fmt, ...)
{
	char buf[256];
	va_list args;
	va_start(args, fmt);

	vsnprintf(buf, sizeof(buf), fmt, args);
	return uart_write(uart, buf, strlen(buf));
}
