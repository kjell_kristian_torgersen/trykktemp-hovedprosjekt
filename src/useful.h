/*
 * useful.h
 *
 *  Created on: Oct 25, 2020
 *      Author: kjell
 */

#ifndef USEFUL_H_
#define USEFUL_H_

void useful_init(void);
int uart_printf(const char *__restrict fmt, ...);

#endif /* USEFUL_H_ */
